//
//  Lab3_task3
//  Babynets Myroslav

import Foundation

func SymbolAnalyser( Symb: Character){
    print("########################################")
    if Symb.isNumber {
        print("This is a number \(Symb)")
    }
    if Symb.isLetter {
        print("This is a letter -> \(Symb)")
        if Symb.isUppercase {
           print("This letter is uppercase")
        }else {
            print("This letter is lowercase")
        }
        let asciiIden = Symb.asciiValue
        if asciiIden != nil {
            
            for i in 0x0041...0x005a {
                if i == asciiIden! {
                    print("This symbol refers to English alphabet")
                }
            }
            for i in 0x0061...0x007a {
                if i == asciiIden!{
                    print("This symbol refers to English alphabet")
                }
            }
        } else {
                print("This symbol refers to Russian alphabet")
    }
    } else {
        print("This is an unknown symbol -> \(Symb)")
    }
}

var symbols = [Character]()
symbols.append(contentsOf: ["1", "v", "9", "Q", "р", "Ф", "7", "+"])

for counter in symbols{
    SymbolAnalyser(Symb: counter)
}

