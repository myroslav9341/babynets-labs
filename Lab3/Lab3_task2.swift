//
//  Lab3_task2
//  Babynets Myroslav


import Foundation

func ArrSorting(Array: inout [Int],_ type: String = "buble" ){
    var temp = 0
    if type == "buble"{
        for i in 0..<Array.count{
            for j in 0..<Array.count-i-1{
                if Array[j] > Array[j + 1] {
                    temp = Array[j]
                    Array[j] = Array[j + 1]
                    Array[j + 1] = temp
                }
            }
        }
        
    }
    if type == "swap" {
        for startInd in 0..<Array.count-1{
            var SmallestInd = startInd
            for currInd in (startInd+1)..<Array.count{
                if Array[currInd] < Array[SmallestInd] { SmallestInd = currInd }
            }
            temp = Array[startInd]
            Array[startInd] = Array[SmallestInd]
            Array[SmallestInd] = temp
        }
    }
    print("Sorted array (method -> \(type)): ", Array)
}


var myArr = [Int]()
var n = 20
for i in 0..<n
{
    myArr.append(( ( i + 3 ) * i ) / 2)
}
myArr = myArr.shuffled()

print("Unsorted array: ", myArr)
ArrSorting(Array: &myArr)
myArr = myArr.shuffled()

print("Unsorted array: ", myArr)
ArrSorting(Array: &myArr, "buble")
myArr = myArr.shuffled()

print("Unsorted array: ", myArr)
ArrSorting(Array: &myArr, "swap")
