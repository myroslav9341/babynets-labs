//
//  Lab3_task6
//  Babynets Myroslav


import Foundation

func dotAttitudeToCircle(_ circles: [Double], x_d: Double, y_d: Double,_ viev: String = "Unvisible") -> Double{
    var Rad: [Double] = []
    var radius: Double = 0
    var i:Int = 0
    var j:Int = 0
    repeat{
        if viev == "Visible"{
            print("Circle \(j) x: ", circles[i], " y: ", circles[i+1], " radius: " ,circles[i+2])
        }
        radius = circles[i + 2]
        if (pow(x_d - circles[i], 2) + pow(y_d - circles[i + 1], 2)) < pow(radius, 2){
            Rad.append(radius)
        }
        i += 3
        j += 1
    }while i < circles.count
    let BiggestCirc = Rad.max() != nil ? Rad.max()! : 0
    return BiggestCirc
    
}

var x:Double = 3
var y:Double = 4
var Circle : [Double] = [2.3,3.4,6.1,   1.2,2.0,11.4,   4.54,3.6,7.45,  10.2,9.5,13.5,   24.5, 17.9, 8.35]

if dotAttitudeToCircle(Circle, x_d: x, y_d: y) != 0{
    print("Biggest circle with radius -> ", dotAttitudeToCircle( Circle, x_d: x, y_d: y, "Visible"))
} else {
    print("Dot with coordinates x: \(x),y: \(y) does not refers to any circle!")
}

