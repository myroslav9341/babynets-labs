//
//  Lab3_task5
//  Babynets Myroslav

import Foundation


func Calc(_ x: Double, _ y: Double, operation: String) -> Double{
    
    var result:Double = -1
    
    switch operation {
    case "+":
        result = x + y
    case "-":
        result = x - y
    case "*":
        result = x * y
    case "%":
        if x < y {
            result = x
        } else {
            let temp = x / y
            let t = temp.rounded(.towardZero) * y
            result = x - t
        }
    case "^":
        let power:Int = Int(y)
        result = x
        for _ in 1..<power{
            result *= x
        }
    default:
        print("Wrong operation")
        result = 0.0
    }
    
    
    return result
}

print("Enter x value from keyboard: ")
var xValue = Double(readLine()!)
print("Enter y value from keyboard: ")
var yValue = Double(readLine()!)

var input = 0
var working = true
while working == true{
    print("1.Summary (x + y).")
    print("2.Substraction (x - y).")
    print("3.Multiplication (x * y).")
    print("4.Rest from dividing (x % y) = ?.")
    print("5.Power (x ^ y).")
    print("6.Exit calc.")
    var input = readLine()
    input = input != nil ? input! : "0"
    
    switch input {
    case "1":
        print("Summary result = ", Calc(xValue!, yValue!, operation: "+"))
    case "2":
        print("Substraction result = ", Calc(xValue!, yValue!, operation: "-"))
    case "3":
        print("Multiplication result = ", Calc(xValue!, yValue!, operation: "*"))
    case "4":
        print("Rest from dividing = ", Calc(xValue!, yValue!, operation: "%"))
    case "5":
        print("x in power y = ", Calc(xValue!, yValue!, operation: "^"))
    case "6":
        working = false
    default:
        print("Wrong input failure(\(String(describing: input)))")
    }
    
}
