//
//  Lab3_task1
//  Babynets Myroslav


import Foundation

func factorial(_ number: Int) -> Int{
    if number < 0 { return 0 }
    if number == 0 { return 1 }
    if number == 1 { return 1 }
    if number > 1 {
        return number * factorial(number - 1)
    } else {
        return 0
    }
}
print(factorial(3))
print(factorial(4))
print(factorial(5))
print(factorial(6))
print(factorial(7))
