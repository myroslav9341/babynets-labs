//
//  Lab3_task4
//  Babynets Myroslav

import Foundation

func StringLineAnalyzer(line: String)
{
    var dLetters: [Character : Int] = [:]
    var lettersSet: Set<Character> = []
    print("Line length is ", line.count)
    let mutLine = line.lowercased()
    for ind in mutLine
    {
        if !ind.isWhitespace{
            lettersSet.insert(ind)
        }
    }
    var counter: Int = 0
    for letter in lettersSet {
        dLetters.updateValue(0, forKey: letter)
        counter += 1
    }
    for letter in lettersSet{
        var iterat = 0
        for t in mutLine {
            if letter == t {
                iterat += 1
                dLetters.updateValue(iterat, forKey: letter)
            }
        }
    }
    for s in lettersSet{
        print(dLetters[dLetters.index(forKey: s)!])
    }
}


var str : String = "Some text in English. All of this letters will be counted.."
StringLineAnalyzer(line: str)
//"This is test line for this project. Only represents that algorithm is working well"