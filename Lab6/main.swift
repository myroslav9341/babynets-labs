//
//  main.swift
//  Babynets Myroslav

import Foundation

class Calculator {
    static let operations = Calculator()
    
    static func plus(_ num1: Rational,_ num2: Rational) -> Rational{
        let result = num1 + num2
        return result
    }
    static func minus(_ num1: Rational,_ num2: Rational) -> Rational{
        let result = num1 - num2
        return result
    }
    func mult(_ num1: Rational,_ num2: Rational) -> Rational{
        let result = num1 * num2
        return result
    }
    func div(_ num1: Rational,_ num2: Rational) -> Rational{
        let result = num1 / num2
        return result
    }
    func copy(_ num: Rational) -> Rational{
        let R = Rational(num.N.m, num.N.n)
        return R
    }
    
    private init() { }
}


var a = Rational(2, 4)
var b = Rational(3, 5)
var c = Rational(-4, 8)

print(Calculator.plus(a, b).N)
print(Calculator.minus(a, b).N)
print(Calculator.operations.mult(a, b).N)
print(Calculator.operations.div(a, b).N)


print(a.N, " > ", b.N, ": ", a > b )
print(b.N, " > ", a.N, ": ", b > a )
print(a.N, " < ", b.N, ": ", a < b )
print(c.N, " < ", a.N, ": ", c < a )
print(b.N, " < ", a.N, ": ", b < a )
print(a.N, " == ", b.N, ": ", a == b )

var testR = Calculator.operations.copy(a)
print("TestR: ", testR.N )