//
//  addoperetion
//  Babynets Myroslav

import Foundation

extension Rational{
    
    static func >(_ left: Rational,_ right: Rational) -> Bool{
        let tmp1:Double = Double(left.N.m) / Double(left.N.n)
        let tmp2:Double = Double(right.N.m) / Double(right.N.n)
        if tmp1 > tmp2{
            return true
        } else {
            return false
        }
    }
    static func <(_ left: Rational,_ right: Rational) -> Bool{
        let tmp1:Double = Double(left.N.m) / Double(left.N.n)
        let tmp2:Double = Double(right.N.m) / Double(right.N.n)
        if tmp1 < tmp2{
            return true
        } else {
            return false
        }
    }
    static func ==(_ left: Rational,_ right: Rational) -> Bool{
        let tmp1:Double = Double(left.N.m) / Double(left.N.n)
        let tmp2:Double = Double(right.N.m) / Double(right.N.n)
        if tmp1 == tmp2{
            return true
        } else {
            return false
        }
    }
    
    static func +(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
        if left.N.n == right.N.n{
            res.N.m = left.N.m + right.N.m
            res.N.n = left.N.n
        } else {
            res.N.m = left.N.m * right.N.n + right.N.m * left.N.n
            res.N.n = left.N.n * right.N.n
        }
        return res
    }
    static func -(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
        if left.N.n == right.N.n{
            res.N.m = left.N.m - right.N.m
            res.N.n = left.N.n
        } else {
            res.N.m = left.N.m * right.N.n - right.N.m * left.N.n
            res.N.n = left.N.n * right.N.n
        }
        return res
    }
    static func *(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
            res.N.m = left.N.m * right.N.m
            res.N.n = left.N.n * right.N.n
        return res
    }
    static func /(_ left: Rational,_ right: Rational) -> Rational{
        let res = Rational(0, 1)
            res.N.m = left.N.m * right.N.n
            res.N.n = left.N.n * right.N.m
        return res
    }
    
}