//
//  Rational
//  Babynets Myroslav

import Foundation


class Rational{
    var N:(m: Int,n: Int) = (0, 1)
    
    init(_ mi: Int,_ ni: Int){
        N.m = mi
        N.n = ni
    }
}
