//
//  clFigures.swift
//  Babynets Myroslav
//

import Foundation


class Line : Figure {
    var name: String?
    var startPoint: Point
    var endPoint: Point
    var vector: (Double, Double){
        return (endPoint.x - startPoint.x, endPoint.y - startPoint.y)
    }
    var distancePoints: Double {
        return sqrt(pow(endPoint.x - startPoint.x, 2) + pow(endPoint.y - startPoint.y, 2))
        }
    var lineModule: Double{
        return sqrt(pow(vector.0,2) + pow(vector.1, 2))
    }
    override var Squre: Double{
        print("This parametr does not exist for figure of type -> Line")
        return 0
    }
    func cosF(with line2: Line) -> (degrees: Double, radians: Double){
        let temp = self.lineModule * line2.lineModule
        var rad = self.ScalarMult(line2) / temp
        var degree = rad / 3.14159265 * 180
        //var degree = 180 / (rad * Double.pi)
       if degree >= 180 {
            rad = (rad - rad.rounded(.towardZero))
            degree = rad / 3.14159265 * 180
        }
        return (degree,rad)
    }
    
    init(_ schemeFigure: Figure){
        startPoint = schemeFigure.Origin[0]
        endPoint = schemeFigure.Origin[1]
        name = schemeFigure.FName
        try! super.init(pointsArray: [startPoint, endPoint], name)
    }
    init(_ stPoint: Point,_ edPoint: Point,_ Name: String?){
        startPoint = stPoint
        endPoint = edPoint
        name = Name
        try! super.init(pointsArray: [stPoint, edPoint], name)
    }
}
class Triangle: Figure {
    var SumOfDegrees = 180.0
    var Torigin = [Point]()
    var name : String?
    override var Squre: Double{
        let a = distance(Point1: Torigin[0], Point2: Torigin[1])
        let b = distance(Point1: Torigin[1], Point2: Torigin[2])
        let c = distance(Point1: Torigin[2], Point2: Torigin[0])
        let p = (a + b + c)/2
        return sqrt(p * (p - a) * (p - b) * (p - c))
    }
    
    init(schemeFigure: Figure) {
        Torigin = schemeFigure.Origin
        name = schemeFigure.FName
        try! super.init(pointsArray: Torigin, name)
    }
    init(_ point1: Point, _ point2: Point, _ point3: Point, _ Name: String?){
       Torigin.append(contentsOf: [point1, point2, point3])
       name = Name != nil ? Name : nil
       try! super.init(pointsArray: Torigin, name)
    }
    
}
class Quadliteral: Figure {
    var Qorigin = [Point]()
    var name : String?
    var SumOfDegree = 360.0
    override var Squre: Double{
        let d = distance(Point1: Qorigin[0], Point2: Qorigin[2])
        let D = distance(Point1: Qorigin[1], Point2: Qorigin[3])
        return (D * d)/2
    }
    init(schemeFigure: Figure) {
        Qorigin = schemeFigure.Origin
        name = schemeFigure.FName
        try! super.init(pointsArray: Qorigin, name)
    }
    init(_ point1: Point,_ point2: Point,_ point3: Point,_ point4: Point,_ Name: String?) {
        Qorigin.append(contentsOf: [point1,point2,point3, point4])
        name = Name != nil ? Name : nil
        try! super.init(pointsArray: Qorigin, name)
    }
    deinit {
        Qorigin.removeAll()
        name = ""
    }
}
