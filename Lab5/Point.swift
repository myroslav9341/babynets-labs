//
//  Point.swift
//  Babynets Myroslav
//

import Foundation

class Point {
    var x:Double = 0.0
    var y:Double = 0.0
     
    init(_ xs: Double?, _ ys: Double?) throws {
        guard xs != nil else {
            print(PointErr.invalidDataForPoint.localizedDescription)
            throw PointErr.invalidDataForPoint
        }
        guard ys != nil else {
            print(PointErr.invalidDataForPoint.localizedDescription)
            throw PointErr.invalidDataForPoint
        }
        x = xs!
        y = ys!
    }
    
    deinit {
        x.self = 0.0
        y.self = 0.0
    }
}
