//
//  clFigure.swift
//  Babynets Myroslav
//

import Foundation


class Figure : Point, FigureDelegate{
    var arrL: [Int] = []
    var Origin = [Point]()
    var FName: String?
    var delegate: FigureDelegate?
    var Perimetr: Double{
        var temp: Double = 0
        for i in 0..<Origin.count{
            if i == Origin.count - 1{
                 temp += distance(Point1: Origin[i], Point2: Origin[0])
            }else{
                temp += distance(Point1: Origin[i], Point2: Origin[i + 1])
            }
        }
            return temp
    }
    var Squre: Double{
        print("Use for specified types of figures")
        return 0
    }
    //delegate
    func stringlengthCounter(figureL: Int) {
        arrL.append(figureL)
    }
    
    init(pointsArray: [Point]?, _ Name: String?) throws
    {
        guard pointsArray != nil else {
            print(FigureErr.missingArrDataInInitSection.localizedDescription)
            throw FigureErr.missingArrDataInInitSection
        }
        Origin = pointsArray!
        FName = Name ?? "Not mentioned"
        try! super.init(0.0, 0.0)
    }
}
