//
//  MathAddons.swift
//  Babynets Myroslav
//

import Foundation


extension Mathmatics{
    func MinMaxPerimetr() -> (min: Double, max: Double, minI: Int, maxI: Int){
        var tempMin:Double = 0
        var tempMax:Double = 0
        var minInd = 0
        var maxInd = 0
        if FigureArray.count != 0{
            tempMin = FigureArray[0].Perimetr
            tempMax = FigureArray[0].Perimetr
            for i in 0..<FigureArray.count{
                if FigureArray[i].Perimetr < tempMin{
                    tempMin = FigureArray[i].Perimetr
                    minInd = i
                }
                if FigureArray[i].Perimetr > tempMax{
                    tempMax = FigureArray[i].Perimetr
                    maxInd = i
                }
            }
        }
        return (tempMin, tempMax, minInd, maxInd)
    }
    func MinMaxSquare() -> (min: Double, max: Double, minI: Int, maxI: Int){
        var tempMin:Double = 0
        var minInd = 0
        var tempMax:Double = 0
        var maxInd = 0
        if FigureArray.count != 0{
            tempMin = FigureArray[0].Squre
            tempMax = FigureArray[0].Squre
            for i in 0..<FigureArray.count{
                if FigureArray[i].Squre > tempMax{
                        tempMax = FigureArray[i].Squre
                        maxInd = i
                    }
                if FigureArray[i].Squre < tempMin{
                        tempMin = FigureArray[i].Squre
                        minInd = i
                }
            }
        }
        return (tempMin, tempMax, minInd, maxInd)
    }
    
    
   static var StringArrAnalyzer : ([Figure]) -> (String, String, String, String) = {figArr in
        var minL,maxL,minSt,maxSt :String
        minL = figArr[0].description; maxL = figArr[0].description
        minSt = figArr[0].description; maxSt = figArr[0].description
        
        for i in 0..<figArr.count{
            if figArr[i].description.distance(from: figArr[i].description.startIndex, to: figArr[i].description.index(before: figArr[i].description.endIndex)) > maxL.distance(from: minL.startIndex, to: minL.index(before: minL.endIndex)){
                maxL = figArr[i].description
            }
            if figArr[i].description.distance(from: figArr[i].description.startIndex, to: figArr[i].description.index(before: figArr[i].description.endIndex)) < minL.distance(from: minL.startIndex, to: minL.index(before: minL.endIndex)){
                minL = figArr[i].description
            }
            if figArr[i].description > maxSt
            {
                maxSt = figArr[i].description
            }
            if figArr[i].description < minSt
            {
                minSt = figArr[i].description
            }
        }
        return (minL, maxL, minSt, maxSt)
    }
    
    func StringDescripAnalyzer(figureArray: [Figure]) -> (minLength: String, maxLength: String, minS: String, maxS: String){
        var minL,maxL,minSt,maxSt :String
        minL = figureArray[0].description; maxL = figureArray[0].description
        minSt = figureArray[0].description; maxSt = figureArray[0].description
        
        for i in 0..<figureArray.count{
            if figureArray[i].description.distance(from: figureArray[i].description.startIndex, to: figureArray[i].description.index(before: figureArray[i].description.endIndex)) > minL.distance(from: minL.startIndex, to: minL.index(before: minL.endIndex)){
                maxL = figureArray[i].description
            }
            if figureArray[i].description.distance(from: figureArray[i].description.startIndex, to: figureArray[i].description.index(before: figureArray[i].description.endIndex)) < minL.distance(from: minL.startIndex, to: minL.index(before: minL.endIndex)){
                minL = figureArray[i].description
            }
            if figureArray[i].description > maxSt
            {
                maxSt = figureArray[i].description
            }
            if figureArray[i].description < minSt
            {
                minSt = figureArray[i].description
            }
        }
        return (minL, maxL, minSt, maxSt)
    }
    
    func showAnalize(_ figureArr: [Figure],_ someClosure: @escaping ([Figure]) -> (String, String, String, String) ){
            print("Shortest string descrp of figue in arr: ", someClosure(figureArr).0)
            print("Longest string descrip of figue in arr: ", someClosure(figureArr).1)
            print("Minimal string descrip of figue in arr:  ", someClosure(figureArr).2)
            print("Maximal string descrip of figue in arr: ", someClosure(figureArr).3)
        delegate?.stringlengthCounter(figureL: someClosure(figureArr).0.count)
        delegate?.stringlengthCounter(figureL: someClosure(figureArr).1.count)
        delegate?.stringlengthCounter(figureL: someClosure(figureArr).2.count)
        delegate?.stringlengthCounter(figureL: someClosure(figureArr).3.count)
    }
    
    
    class func applyKTimes(rep: Int, _ closure: emptyClosure) throws {
        guard rep >= 2 else {
            print(MathErr.nonRepeatResult.localizedDescription)
            throw MathErr.nonRepeatResult
        }
        for _ in 0..<rep {
            closure()
        }
    }
    
    class func MaxInArr(arrayInt: [Int]) -> Int{
        return arrayInt.reduce(arrayInt[0]) {
            ($0 > $1) ? $0 : $1
        }
    }
    
    class func StringAdder(strArr: [String]){
        print()
        let strLine = strArr.reduce("", {x, y in x + y })
        print("strArr \(strArr) in string line view -> ", strLine)
    }
    
    static func forEach(array: [Int], _ closure:(Int) -> ()){
        for i in 0..<array.count {
            print("Arr element #\(i) -> ")
            closure(array[i])
        }
    }
    
}
