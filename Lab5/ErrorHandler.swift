//
//  ErrorHandler.swift
//  Babynets Myroslav
//

import Foundation

enum PointErr {
    case invalidDataForPoint, unknownErrP
}
enum FigureErr {
    case missingArrDataInInitSection, unknownErrF
}
enum MathErr {
    case invalidDataForMath, unknownErrM, nonRepeatResult
}

extension PointErr: LocalizedError{
    var errorDescription: String?{
        switch self{
        case .invalidDataForPoint:
            return NSLocalizedString("Data for point is missing or invalid", comment: "")
        case .unknownErrP:
            return NSLocalizedString("Unknown error :(", comment: "")
        }
    }
}
extension FigureErr: LocalizedError{
    var errorDescription: String?{
        switch self{
        case .missingArrDataInInitSection:
            return NSLocalizedString("Given array is Empty. Unable to create figure", comment: "")
        case .unknownErrF:
            return NSLocalizedString("Unknown error :(", comment: "")
        }
    }
}
extension MathErr: LocalizedError{
    var errorDescription: String?{
        switch self{
        case .invalidDataForMath:
            return NSLocalizedString("Data for Math is missing or invalid", comment: "")
        case .unknownErrM:
            return NSLocalizedString("Unknown error :(", comment: "")
        case .nonRepeatResult:
            return NSLocalizedString("Entered number does not confirm to repeating task", comment: "")
        }
    }
}
