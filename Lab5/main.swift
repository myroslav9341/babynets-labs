//
//  main.swift
//  Babynets Myroslav
//

import Foundation

var P1 = try Point(2.4, 3.15)
var P2 = try Point(5, 4)
var P3 = try Point(3, 6)
var P4 = try Point(2, 4)
var P5 = try Point(7.32, 6.15)
var P6 = try Point(4, 9)

var PointsArray = [Point]()
PointsArray.append(P1)
PointsArray.append(P2)
PointsArray.append(P3)

var schemeT1 = try Figure(pointsArray: [P6,P3,P4], "Triangle2")
var schemeT2 = try Figure(pointsArray: [P2,P4], "LINE1")
var ln1 = Line(schemeT2)
var tri2 = Triangle(schemeFigure: schemeT1)
var ln2 = Line(P3, P2, "Line2")

var schemeT3 = try Figure(pointsArray: [P6,P2,P3,P4], "quadl_test")
var quad1 = Quadliteral(schemeFigure: schemeT3)

print(quad1.name!, " perimetr is ", String(format: "%.4f", quad1.Perimetr))
print(tri2.name!, " perimetr is ", String(format: "%.4f", tri2.Perimetr))

print("Scalar mult =", String(format: "%.4f", ln1.ScalarMult(ln2)))
print("Cos Fi between ln1 and ln2(in degrees):", String(format: "%.4f", ln1.cosF(with: ln2).degrees), " and (in radians)", String(format: "%.4f", ln1.cosF(with: ln2).radians))

PointsArray = []

var P01 = try Point(1, 3)
var P02 = try Point(1, 1)
var P03 = try Point(2, 1)
var P04 = try Point(4, 2.73322222)
var P05 = try Point(3, 1)
var P06 = try Point(5, 1)
var P07 = try Point(3, -1)
var P08 = try Point(1, -2)
var P09 = try Point(5, -2)

var TriArr1 = [Point]()
TriArr1.append(contentsOf: [P01, P02, P03])
var schemeT4 = try Figure(pointsArray: TriArr1, "rect_test")
var rect_t = Triangle(schemeFigure: schemeT4)
TriArr1 = []
TriArr1.append(contentsOf: [P04, P05, P06])
var schemeT5 = try Figure(pointsArray: TriArr1, "eq_sided_test")
var eq_s_t = Triangle(schemeFigure: schemeT5)
TriArr1 = []
TriArr1.append(contentsOf: [P07, P08, P09])
var schemeT6 = try Figure(pointsArray: TriArr1, "2eq_sided_test")
var two_eq_s_t = Triangle(schemeFigure: schemeT6)
TriArr1 = []


print("""
\nTypes of shape:
\(rect_t.name!) of type \( rect_t.typeT_S)
\(eq_s_t.name!) of type \( eq_s_t.typeT_S)
\(two_eq_s_t.name!) of type \( two_eq_s_t.typeT_S)

Type of angles:
\(rect_t.name!) of type \( rect_t.typeT_A)
\(eq_s_t.name!) of type \( eq_s_t.typeT_A)
\(two_eq_s_t.name!) of type \( two_eq_s_t.typeT_A)

""")
var n: String? = nil
var Q1 = try Point(1,6)
var Q2 = try Point(1,4)
var Q3 = try Point(3,4)
var Q4 = try Point(3,6)
var quadrat1 = Quadrat([Q1,Q2,Q3,Q4], "quadrat test name")
var Q5 = try Point(1,3)
var Q6 = try Point(1,1)
var Q7 = try Point(4,1)
var Q8 = try Point(4,3)
var quadangle1 = Quadangled([Q5,Q6,Q7,Q8], "quadangle test name")
var Q9 = try Point(6,6)
var Q10 = try Point(5,4)
var Q11 = try Point(6,2)
var Q12 = try Point(7,4)
var romb1 = Romb([Q9,Q10,Q11,Q12], "romb test name")

print("""
\nTypes of shape:
\(romb1.name!) of type \( romb1.qType)
\(quadrat1.name!) of type \( quadrat1.qType)
\(quadangle1.name != nil ? quadangle1.name! : "unnamed") of type \( quadangle1.qType)
""")


var FArr = [Figure]()
FArr.append(contentsOf: [romb1, rect_t, eq_s_t,quadrat1, quadangle1, two_eq_s_t])
let figureDelegate = try Figure(pointsArray: [P01, P02, P03], n)
var MathSpecs = try Mathmatics(figureArr: FArr, figureDelegate)
MathSpecs.showAnalize(FArr, Mathmatics.StringArrAnalyzer)


print("\n Minimal Perimetr has ", MathSpecs.FigureArray[MathSpecs.MinMaxPerimetr().minI].FName != nil ? MathSpecs.FigureArray[MathSpecs.MinMaxPerimetr().minI].FName! : "unnamed", " and it equals ", MathSpecs.MinMaxPerimetr().min,"\n",
      "Maximal Perimetr has ", MathSpecs.FigureArray[MathSpecs.MinMaxPerimetr().maxI].FName != nil ? MathSpecs.FigureArray[MathSpecs.MinMaxPerimetr().maxI].FName! : "unnamed", " and it equals ", MathSpecs.MinMaxPerimetr().max,"\n",
      "Minimal Square has ", MathSpecs.FigureArray[MathSpecs.MinMaxSquare().minI].FName != nil ? MathSpecs.FigureArray[MathSpecs.MinMaxSquare().minI].FName! : "unnamed", " and it equals ", MathSpecs.MinMaxSquare().min,"\n",
      "Maximal Square has ", MathSpecs.FigureArray[MathSpecs.MinMaxSquare().maxI].FName != nil ? MathSpecs.FigureArray[MathSpecs.MinMaxSquare().maxI].FName! : "unnamed", " and it equals ", MathSpecs.MinMaxSquare().max)


print("Lenght of string desc of figures, that were analyzed by closure: ", figureDelegate.arrL)


try Mathmatics.applyKTimes(rep: 4){
    print("repeat")
}

var ArrOfInt:[Int] = [4,7,12,1,53,6,0,17]
print("Max element in array \(ArrOfInt) -> ", Mathmatics.MaxInArr(arrayInt: ArrOfInt))

let StringArray = ["This ","would be ","one ","independent sentence"]

Mathmatics.StringAdder(strArr: StringArray)


Mathmatics.forEach(array: ArrOfInt){ print($0) }


//var xt: Double? = nil
//var p21 = try Point(xt,0.5)

//var tArr:[Point]? = nil
//var t_Fig = try Figure(pointsArray: tArr, "test")

//var FArrT:[Figure]? = nil
//var mathT = try Mathmatics(figureArr: FArrT, figureDelegate)
