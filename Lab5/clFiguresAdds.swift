//
//  clFiguresAdds.swift
//  Babynets Myroslav
//

import Foundation


class Romb: Quadliteral {
    var RQorigin = [Point]()
    var Rname: String?
    
    init(_ quad: Quadliteral, _ Name: String?) {
        super.init(schemeFigure: quad)
        RQorigin.append(contentsOf: quad.Qorigin)
        Rname = Name
    }
    init(_ pointsArr: [Point],_ Name: String?) {
        super.init(pointsArr[0], pointsArr[1], pointsArr[2], pointsArr[3], Name)
        RQorigin = pointsArr
        Rname = Name
    }
}
class Quadrat: Quadliteral {
    var QQorigin = [Point]()
    var Qname: String?
        
    init(_ quad: Quadliteral, _ Name: String?) {
        super.init(schemeFigure: quad)
        QQorigin.append(contentsOf: quad.Qorigin)
        Qname = Name
    }
    init(_ pointsArr: [Point],_ Name: String?) {
        super.init(pointsArr[0], pointsArr[1], pointsArr[2], pointsArr[3], Name)
        QQorigin = pointsArr
        Qname = Name
    }
}
class Quadangled: Quadliteral {
    var QAQorigin = [Point]()
    var QAname: String?
    
    init(_ quad: Quadliteral, _ Name: String?) {
        super.init(schemeFigure: quad)
        QAQorigin.append(contentsOf: quad.Qorigin)
        QAname = Name
    }
    init(_ pointsArr: [Point],_ Name: String?) {
        super.init(pointsArr[0], pointsArr[1], pointsArr[2], pointsArr[3], Name)
        QAQorigin = pointsArr
        QAname = Name
    }
}
