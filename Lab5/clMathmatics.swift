//
//  clMathmatics.swift
//  Babynets Myroslav
//

import Foundation


class Mathmatics: MathProtocol{
    typealias emptyClosure = () -> ()
    
    var FigureArray = [Figure]()
    var delegate: FigureDelegate?
    

    required init(figureArr: [Figure]?,_ delegate: FigureDelegate?) throws
    {
        guard figureArr != nil else {
            print(MathErr.invalidDataForMath.localizedDescription)
            throw MathErr.invalidDataForMath
        }
        FigureArray = figureArr!
        self.delegate = delegate
    }
    deinit {
        FigureArray.removeAll()
    }
}
