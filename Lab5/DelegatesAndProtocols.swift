//
//  Delegates.swift
//  Babynets Myroslav
//

import Foundation

protocol FigureDelegate {
    var arrL:[Int] {set get}
    func stringlengthCounter(figureL: Int)
}

protocol MathProtocol {
    var FigureArray: [Figure] {get set}
    var delegate: FigureDelegate? {get set}
    
    init(figureArr: [Figure]?,_ delegate: FigureDelegate?) throws
}
