//
//  FigureSpecs.swift
//  Babynets Myroslav
//

import Foundation

extension Figure{
    
    var description:String{
        var desc : String = ""
        var temp1 : String = ""
        var temp2 : String = ""
        for i in 0..<Origin.count {
            temp1 = String(format: "%.2f", Origin[i].x)
            temp2 = String(format: "%.2f", Origin[i].y)
            desc += "\nPoint #" + String(i) + " (" + temp1 + "; " + temp2 + ")"
        }
    return desc
    }
    
}

extension Line{
    func ScalarMult(_ line: Line ) -> Double
    {
        let scalar = (self.startPoint.x * line.endPoint.x) + (self.startPoint.y * line.endPoint.y)
        return scalar
    }
}

extension Triangle{
    
    var typeT_S: sideLength{
        
        let A = distance(Point1: Torigin[0], Point2: Torigin[1])
        let B = distance(Point1: Torigin[1], Point2: Torigin[2])
        let C = distance(Point1: Torigin[2], Point2: Torigin[0])
       // print("A = ",A," B = ",B," C = ", C)
        if(A == B && A == C){
            return .equalSided
        }
        if (A == B || A == C){
            return .TwoSidesEqual
        }
        if ( A != B && A != C){
            return .NoEqualSides
        }
        return .Unknown
    }
    var typeT_A: typeAngle{
        let a = Line(Torigin[0], Torigin[1], "a")
        let b = Line(Torigin[1], Torigin[2], "b")
        let c = Line(Torigin[2], Torigin[0], "c")
        //print(a.cosF(with: b).degrees, b.cosF(with: c).degrees,  c.cosF(with: a).degrees)
        if a.cosF(with: b).degrees == 90 || b.cosF(with: c).degrees == 90 || c.cosF(with: a).degrees == 90{
            return .rectangular
        }
        if a.cosF(with: b).degrees < 90 && b.cosF(with: c).degrees < 90 && c.cosF(with: a).degrees < 90 {
            return .acute_angle
        }
        if a.cosF(with: b).degrees > 90 || b.cosF(with: c).degrees > 90 || c.cosF(with: a).degrees > 90{
            return .obtuse
        }
        
        return .unknown
    }
    enum typeAngle {
        case obtuse, rectangular, acute_angle, unknown
    }
    enum sideLength{
        case equalSided, TwoSidesEqual, NoEqualSides, Unknown
    }
    
    
}

extension Quadliteral{
    
    enum quadType{
        case qRomb, qQuadrat, qQuadangled, qElse
    }
    var qType:quadType{
        let aq = Line(Qorigin[0], Qorigin[1], "a")
        let bq = Line(Qorigin[1], Qorigin[2], "b")
        let cq = Line(Qorigin[2], Qorigin[3], "c")
        let dq = Line(Qorigin[3], Qorigin[0], "d")
        
        let e = Line(Qorigin[0], Qorigin[2], "e")
        let u = Line(Qorigin[1], Qorigin[3], "u")
        //print("aq=",aq.distancePoints, " bq=", bq.distancePoints, " cq=", cq.distancePoints, " dq=", dq.distancePoints, "||||  e =", e.distancePoints, " u =", u.distancePoints)
        if (aq.distancePoints == cq.distancePoints && bq.distancePoints == dq.distancePoints) && (aq.distancePoints != bq.distancePoints){
            return .qQuadangled
        }
        if (aq.distancePoints == cq.distancePoints && bq.distancePoints == dq.distancePoints) && (aq.distancePoints == bq.distancePoints) {
            if e.distancePoints == u.distancePoints  {
                return .qQuadrat
            } else {
            return .qRomb
            }
        }
        return .qElse
    }
}

func distance(Point1: Point, Point2: Point) -> Double{
    let d = sqrt(pow(Point2.x - Point1.x, 2) + pow(Point2.y - Point1.y, 2))
    return d
}
