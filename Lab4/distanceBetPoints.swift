//
//  distanceBetPoints.swift
//  Babynets Myroslav
//

import Foundation

extension PolarPoint{
    func distance(Point1: PolarPoint, Point2: PolarPoint) -> Double{
        let x_t1 = Point1.SetGetRad * cos(Point1.SetGetDeg)
        let y_t1 = Point1.SetGetRad * sin(Point1.SetGetDeg)
        let x_t2 = Point2.SetGetRad * cos(Point2.SetGetDeg)
        let y_t2 = Point2.SetGetRad * sin(Point2.SetGetDeg)

        let d = sqrt(pow(x_t2 - x_t1, 2) + pow(y_t2 - y_t1, 2))
        return d
    }
}
extension CartesianPoint{
    func distance(Point1: CartesianPoint, Point2: CartesianPoint) -> Double{
        let d = sqrt(pow(Point2.SetGetX - Point1.SetGetX, 2) + pow(Point2.SetGetY - Point1.SetGetY, 2))
        return d
    }
}
func distance(Point1: CartesianPoint, Point2: CartesianPoint) -> Double{
    let d = sqrt(pow(Point2.SetGetX - Point1.SetGetX, 2) + pow(Point2.SetGetY - Point1.SetGetY, 2))
    return d
}
func distance(Point1: CartesianPoint, Point2: PolarPoint) -> Double{
    
    let x_t = Point2.SetGetRad * cos(Point2.SetGetDeg)
    let y_t = Point2.SetGetRad * sin(Point2.SetGetDeg)
    
    let d = sqrt(pow(x_t - Point1.SetGetX, 2) + pow(y_t - Point1.SetGetY, 2))
    return d
}
func distance(Point1: PolarPoint, Point2: CartesianPoint) -> Double{
    
    let x_t = Point1.SetGetRad * cos(Point1.SetGetDeg)
    let y_t = Point1.SetGetRad * sin(Point1.SetGetDeg)
    
    let d = sqrt(pow(Point2.SetGetX - x_t, 2) + pow(Point2.SetGetY - y_t, 2))
    return d
}

