//
//  main.swift
//  Babynets Myroslav
//

import Foundation


struct CartesianPoint{
    private var x = 0.0
    private var y = 0.0
    
    var SetGetX:Double{
        get{
            return x
        }
        set(newX){
            x = newX
        }
    }
    var SetGetY:Double{
        get{
            return y
        }
        set(newY){
            y = newY
        }
    }
    
    init() {}
    init(x: Double, y:Double) {
        self.x = x
        self.y = y
    }
    init(fromPolarCoord radius:Double, degree: Double) {
        x = radius * cos(degree)
        y = radius * sin(degree)
    }
}

struct PolarPoint{
    var radius = 0.0
    var SetGetRad:Double{
        get{
            return radius
        }
        set(newRadius){
            radius = newRadius
        }
    }
    var degree = 0.0
    var SetGetDeg:Double{
        get{
            return degree
        }
        set(newDegree){
            degree = newDegree
        }
    }
    init() {}
    init(radius: Double, degree:Double) {
        self.radius = radius
        self.degree = degree
    }
    init(fromCartesianCoord x: Double, y: Double){
        radius = sqrt(pow(x,2) + pow(y, 2))
        let tmp = x / radius
        degree = acos(tmp)   //radians
        //= temp / 3.14159265 * 180
    }
}

struct Figure{
    private var origin = [CartesianPoint]()
    enum FType {
        case undified, triangle, quadrilateral, polygon, point, line
    }
    var SetGetOrigin: [CartesianPoint]{
        set(newOrigin){
            origin = newOrigin
        }
        get{
            return origin
        }
    }
    var type: FType{
        if origin.count == 1 {
            return .point
        }
        if origin.count == 2{
            if origin[0].SetGetX != origin[1].SetGetX || origin[0].SetGetY != origin[1].SetGetY{
                return .line
            }
        }
        if origin.count == 3 {
            if (origin[0].SetGetX != origin[1].SetGetX || origin[0].SetGetY != origin[1].SetGetY) && (origin[2].SetGetX != origin[0].SetGetX || origin[2].SetGetY != origin[0].SetGetY) {
                return .triangle
            }
        }
        if origin.count == 4 {
            return .quadrilateral
            }
        if origin.count >= 5{
            return .polygon
        }
        return .undified
    }
    
    init(fromCartesianPointsArray array: [CartesianPoint]) {
        origin = array;
    }
    
    subscript(index: Int) -> CartesianPoint?{
        if origin.count < index{
            return nil
        }
        return origin[index]
    }
    
}

func PointDescription(point: CartesianPoint){
    print("X: ", String(format: "%.4f", point.SetGetX), "Y: ", String(format: "%.4f", point.SetGetY))
}
func PointDescription(point: PolarPoint){
    print("Radius: ", String(format: "%.4f", point.SetGetRad), "Degree: ", String(format: "%.4f", point.SetGetDeg))
}

var PointC1 = CartesianPoint(x: 4.1, y: 6.3)
print("X: ",PointC1.SetGetX, "Y: ", PointC1.SetGetY)
PointC1.SetGetX = 10 ; PointC1.SetGetY = 5
print("X: ", PointC1.SetGetX, "Y: ", PointC1.SetGetY)

var PointC2 = CartesianPoint(fromPolarCoord: 6.4, degree: 0.896)
print("X: ", String(format: "%.3f", PointC2.SetGetX), "Y: ", String(format: "%.3f", PointC2.SetGetY))


var PointP1 = PolarPoint.init(fromCartesianCoord: 8, y: 9)
print("Radius: ", String(format: " %.2f", PointP1.SetGetRad)," Degree: ", String(format: " %.4f", PointP1.SetGetDeg))

PointC2.SetGetX = 5
PointC2.SetGetY = 10
print("X: ", PointC2.SetGetX, "Y: ", PointC2.SetGetY)

PointDescription(point: PointC1)
PointDescription(point: PointP1)

var PointP2 = PolarPoint(radius: 3.6, degree: 0.755)

print("\n")
print("Distance between PointP1 and PointP2 = ", String(format: "%.4f", PointP1.distance(Point1: PointP1, Point2: PointP2)))
print("Distance between PointP1 and PointC1 = ", String(format: "%.4f", distance(Point1: PointP1, Point2: PointC1)))
print("Distance between PointC2 and PointP2 = ", String(format: "%.4f", distance(Point1: PointC2, Point2: PointP2)))
print("Distance between PointC2 and PointC1 = ", String(format: "%.4f", PointC1.distance(Point1: PointC1, Point2: PointC2)))

print(PointC1.StringView())
print(PointP1.StringView())

var PointC3 = CartesianPoint(x: 3.43, y: 6.18)
var PointC4 = CartesianPoint(x: 4.27, y: 13.04)
var PointC5 = CartesianPoint(x: 1.35, y: 8.68)
var PointC6 = CartesianPoint(x: 4.65, y: 7.99)
var PointC7 = CartesianPoint(x: 11.32, y: 5.43)
var PointC8 = CartesianPoint(x: 10.01, y: 9.21)

var TParray:[CartesianPoint] = [PointC1, PointC2, PointC3]
var triangle1 = Figure(fromCartesianPointsArray: TParray)

print("Figure #1 -> ", triangle1.type)

TParray.removeLast()
var line1 = Figure(fromCartesianPointsArray: TParray)
print("Figure #2 -> ", line1.type)

TParray.append(PointC4)
TParray.append(PointC5)

var quad = Figure(fromCartesianPointsArray: TParray)
print("Figure #3 -> ", quad.type)

TParray.append(PointC6)
TParray.append(PointC7)

var poly = Figure(fromCartesianPointsArray: TParray)
print("Figure #4 -> ", poly.type)


print(poly[0] != nil ? poly[0]! : "No point at given index")
print(triangle1[4] != nil ? poly[0]! : "No point at given index")

print("Periment of triangle = ", String(format: "%.4f", triangle1.Perimetr()))
print("Periment of quad = ", String(format: "%.4f", quad.Perimetr()))
print("Perimetr of quad property = ", String(format: "%.4f", quad.PropPerimentr))

print("Square of quad = ", String(format: "%.4f", quad.Square()))
print("Square of quad property = ", String(format: "%.4f", quad.PropSqure))
print("Square of poly property = ", String(format: "%.4f", poly.PropSqure))
