//
//  PointPresentation.swift
//  Babynets Myroslav
//

import Foundation

extension CartesianPoint{
    func StringView() -> String{
        var view: String
        view = "View of Cartesian Point -> x: " + String(format: "%.4f", self.SetGetX) + " y: " + String(format: "%.4f", self.SetGetY)
        return view
    }
}

extension PolarPoint{
    func StringView() -> String{
        var view: String
        view = "View of Polar Point -> radius: " + String(format: "%.4f", self.SetGetRad) + " degree: " + String(format: "%.4f", self.SetGetDeg)
        return view
    }
}
