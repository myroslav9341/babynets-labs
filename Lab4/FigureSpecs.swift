//
//  FigureSpecs.swift
//  Babynets Myroslav
//

import Foundation


extension Figure{
    
    var PropPerimentr: Double{
        var FPerimetr: Double = 0
        if type == .line || type == .point || type == .undified{
            print("This figures does not have such specs.")
            return -1
        }else {
            for i in 0..<SetGetOrigin.count{
                if i == SetGetOrigin.count - 1{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[0])
                }else{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[i + 1])
                }
            }
        }
        return FPerimetr
    }
    
    var PropSqure: Double{
        var FSquare:Double = 0
        if type == .line || type == .point{
            print("This figures does not have such specs.")
            return -1
        } else {
            if type == .triangle{
                let a = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[1])
                let b = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[2])
                let c = distance(Point1: SetGetOrigin[2], Point2: SetGetOrigin[0])
                let p = (a + b + c)/2
                FSquare = sqrt(p * (p - a) * (p - b) * (p - c))
            }
            if type == .quadrilateral{
                let d = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[2])
                let D = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[3])
                FSquare = (D * d)/2
            }
            if type == .polygon{
                let halfIndex:Int = (SetGetOrigin.count / 2)
                let temp = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[halfIndex])
                FSquare = 1/2 * self.Perimetr() * temp
            }
        }
        
        return FSquare
    }
    
    func Perimetr() -> Double{
        var FPerimetr: Double = 0
        if type == .line || type == .point || type == .undified{
            print("This figures does not have such specs.")
            return -1
        }else {
            for i in 0..<SetGetOrigin.count{
                if i == SetGetOrigin.count - 1{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[0])
                }else{
                    FPerimetr += distance(Point1: SetGetOrigin[i], Point2: SetGetOrigin[i + 1])
                }
            }
        }
        return FPerimetr
    }
    
    func Square() -> Double{
        var FSquare:Double = 0
        if type == .line || type == .point{
            print("This figures does not have such specs.")
            return -1
        } else {
            if type == .triangle{
                let a = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[1])
                let b = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[2])
                let c = distance(Point1: SetGetOrigin[2], Point2: SetGetOrigin[0])
                let p = (a + b + c)/2
                FSquare = sqrt(p * (p - a) * (p - b) * (p - c))
            }
            if type == .quadrilateral{
                let d = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[2])
                let D = distance(Point1: SetGetOrigin[1], Point2: SetGetOrigin[3])
                FSquare = (D * d)/2
            }
            if type == .polygon{
                let halfIndex:Int = (SetGetOrigin.count / 2)
                let temp = distance(Point1: SetGetOrigin[0], Point2: SetGetOrigin[halfIndex])
                FSquare = 1/2 * self.Perimetr() * temp
            }
        }
        
        return FSquare
    }
    
}
